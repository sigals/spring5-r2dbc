Spring 5 workshop with R2DBC
-----
This repository is a version of a Spring 5 workshop [repository](https://github.com/sshahar1/Spring5Workshop) I gave in 2018.

The difference between both repositories are:
1. The database has changed from MongoDb to H2
2. To work reactively with the H2 database, R2DBC is used
3. Programming language has changed from Java to Kotlin
4. Build has changed from Maven to Gradle
