package com.tikalk.workshop.repository

import com.tikalk.workshop.entity.Person
import org.springframework.data.r2dbc.repository.R2dbcRepository
import org.springframework.stereotype.Repository
import java.math.BigInteger

@Repository
interface PersonRepository : R2dbcRepository<Person, BigInteger>
