package com.tikalk.workshop.app

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories
import org.springframework.web.reactive.config.EnableWebFlux

import org.springframework.data.r2dbc.connectionfactory.init.ConnectionFactoryInitializer

import io.r2dbc.spi.ConnectionFactory

import org.springframework.context.annotation.Bean
import org.springframework.core.io.ByteArrayResource
import org.springframework.data.r2dbc.connectionfactory.init.ResourceDatabasePopulator


@SpringBootApplication
@ComponentScan("com.tikalk.workshop.controller", "com.tikalk.workshop.service")
@EnableR2dbcRepositories(basePackages = ["com.tikalk.workshop.repository"])
@EnableWebFlux
open class WorkshopApp {

    @Bean
    open fun initializer(connectionFactory: ConnectionFactory): ConnectionFactoryInitializer? {
        val initializer = ConnectionFactoryInitializer()
        initializer.setConnectionFactory(connectionFactory)
        initializer.setDatabasePopulator(
            ResourceDatabasePopulator(
                ByteArrayResource(
                    ("DROP TABLE IF EXISTS person;"
                            + "CREATE TABLE person (id IDENTITY NOT NULL PRIMARY KEY, given_name VARCHAR(100) NOT NULL, sure_name VARCHAR(100) NOT NULL);")
                        .toByteArray()
                )
            )
        )
        return initializer
    }
}

fun main(args: Array<String>) {
    runApplication<WorkshopApp>(*args)
}
