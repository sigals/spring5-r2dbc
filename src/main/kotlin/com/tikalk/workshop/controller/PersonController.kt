package com.tikalk.workshop.controller

import com.tikalk.workshop.entity.Person
import com.tikalk.workshop.service.PersonService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux

/**
 * Reactive REST controller for persons
 */
@RestController
@RequestMapping(value = ["/person"], produces = [MediaType.APPLICATION_JSON_VALUE])
class PersonController(private val personService: PersonService) {

    companion object {
        private val LOG = LoggerFactory.getLogger(PersonController::class.java)
    }

    @PostMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun postPersons(@RequestBody persons: Flux<Person>): Flux<Person> {
        LOG.info("Received request")
        val created: Flux<Person> = personService.storePersonsFlux(persons)
        LOG.info("Request processed")
        return created
    }

    @GetMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
    fun all(): Flux<Person> {
        return personService.getAll()
    }

}
