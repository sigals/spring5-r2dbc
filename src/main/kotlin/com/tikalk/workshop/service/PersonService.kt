package com.tikalk.workshop.service;

import com.tikalk.workshop.entity.Person;
import com.tikalk.workshop.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import java.util.stream.Collectors;

/**
 * Reactive service
 */
@Service
class PersonService @Autowired constructor(val personRepository: PersonRepository) {

    fun storePersonsFlux(persons: Flux<Person>):Flux<Person> {
        return personRepository.saveAll(persons);
    }

    fun getAll(): Flux<Person>{
        return personRepository.findAll();
    }
}
