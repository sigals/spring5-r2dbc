package com.tikalk.workshop.entity

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table

@Table("person")
data class Person(
    @Id
    @Column
    var id: Long? = null,
    @Column
    var givenName: String,
    @Column
    var sureName: String)
